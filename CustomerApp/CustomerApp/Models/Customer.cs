﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace CustomerApp.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        public string firstName { get; set; }
        public string surname { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int phoneNumber { get; set; }
    }
}